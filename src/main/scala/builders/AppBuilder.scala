package builders

import cats.effect.IO
import org.http4s._
import org.http4s.server.Router

import http.routes.ExchangeRoutes
import services.CurrencyExchange

class AppBuilder private(
    exchangeService: CurrencyExchange
) {
  private val API_PREFIX = "/api"

  private val exchangeRoutes =
    ExchangeRoutes.make(exchangeService)

  private val routes: HttpRoutes[IO] = Router(
    API_PREFIX -> exchangeRoutes.routes
  )

  val httpApp: HttpApp[IO] =
    routes.orNotFound
}

object AppBuilder {
  def make(exchangeService: CurrencyExchange): IO[AppBuilder] = {
    IO.delay(
      new AppBuilder(exchangeService)
    )
  }
}
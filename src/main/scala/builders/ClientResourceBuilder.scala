package builders

import cats.effect.IO
import cats.effect.kernel.Resource
import org.http4s.blaze.client.BlazeClientBuilder
import org.http4s.client.Client

import config.AllConfigs.GeneralClientConfig

object ClientResourceBuilder {
  def makeClientResource(cfg: GeneralClientConfig): Resource[IO, Client[IO]] =
    BlazeClientBuilder[IO]
      .withConnectTimeout(cfg.connectTimeout)
      .withRequestTimeout(cfg.requestTimeout)
      .resource
}

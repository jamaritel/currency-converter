import cats.effect._
import org.http4s.blaze.server.BlazeServerBuilder
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger

import builders._
import config.ConfigLoader
import http.clients.PrivatBankClientService
import services.CurrencyExchangeService

object Main extends IOApp {

  implicit val logger: SelfAwareStructuredLogger[IO] = Slf4jLogger.getLogger[IO]

  def run(args: List[String]): IO[ExitCode] =
    ConfigLoader.load().flatMap { cfg =>
      ClientResourceBuilder.makeClientResource(cfg.clientConfig).use { client =>
        for {
          pbClient <- PrivatBankClientService.make(
            client,
            cfg.pbConfig
          )
          exchangeService <- CurrencyExchangeService.make(pbClient, cfg.retryConfig)
          appBuilder <- AppBuilder.make(exchangeService)
          _ <- BlazeServerBuilder[IO]
            .bindHttp(cfg.serverConfig.port, cfg.serverConfig.host)
            .withHttpApp(appBuilder.httpApp)
            .serve
            .compile
            .drain
        } yield ExitCode.Success
      }
    }

}

package http.routes

import cats.effect._

import org.http4s._
import org.http4s.circe.toMessageSyntax
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import http.JsonCodecs._

import scala.util._

import services.CurrencyExchange
import domain.Currencies._

class ExchangeRoutes private(
    currencyExchangeService: CurrencyExchange
) extends Http4sDsl[IO] {

  private val prefixPath = "/exchange"

  implicit val dateParamDecoder: QueryParamDecoder[DateParam] =
    QueryParamDecoder[String]
      .emap { s =>
        val df = new java.text.SimpleDateFormat("dd.MM.yyyy")

        Try(df.parse(s)) match {
          case Success(_) => Right(DateParam.fromString(s))
          case Failure(_) => Left(ParseFailure("Invalid date format", ""))
        }
      }

  object DateQueryParam extends QueryParamDecoderMatcher[DateParam]("date")

  private val httpRoutes: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case req @ POST -> Root / "cash" =>
      req.asJsonDecode[RequestData]
        .flatMap { (data: RequestData) =>
           currencyExchangeService.convertCash(data)
        }
        .flatMap(Ok(_))

    case req @ POST -> Root / "cashless" =>
      req.asJsonDecode[RequestData]
        .flatMap { (data: RequestData) =>
          currencyExchangeService.convertCashless(data)
        }
        .flatMap(Ok(_))

    case req @ POST -> Root / "archive" :? DateQueryParam(date) =>
      req.asJsonDecode[RequestData]
        .flatMap { (data: RequestData) =>
          currencyExchangeService.convertForDate(data, date)
        }
        .flatMap(Ok(_))

  }

  val routes: HttpRoutes[IO] = Router(
    prefixPath -> httpRoutes
  )
}

object ExchangeRoutes {
  def make(currencyExchange: CurrencyExchange): ExchangeRoutes =
    new ExchangeRoutes(currencyExchange)
}

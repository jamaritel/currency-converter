package http

import cats.Applicative
import io.circe._
import org.http4s.EntityEncoder
import org.http4s.circe.jsonEncoderOf

import domain.Currencies._

object JsonCodecs {
  // ------------ FIELDS NAMES CONSTANTS ------------
  val FROM_FIELD_NAME = "from"
  val TO_FIELD_NAME = "to"
  val AMOUNT_FIELD_NAME = "amount"
  val CONVERTED_FIELD_NAME = "converted"

  // ------------ PRIVATBANK API FIELDS -------------
  val CCY_PB = "ccy"
  val BASE_PB = "base_ccy"
  val BUY_PB = "buy"
  val SALE_PB = "sale"


  // ---------------- ENTITY ENCODER ----------------
  implicit def deriveEntityEncoder[F[_]: Applicative, A: Encoder]: EntityEncoder[F, A] = jsonEncoderOf[F, A]


  // ----- REQUEST AND RESPONSE FIELDS TO DOMAIN -----
  implicit val moneyReprDecoder: Decoder[MoneyRepr] =
    Decoder.instance { (c: HCursor) =>
      c.value.as[String].map(MoneyRepr.fromString)
    }

  implicit val currCodeDecoder: Decoder[CurrCode] =
    Decoder.instance { (c: HCursor) =>
      c.value.asString.flatMap(CurrCode.convertString) match {
        case Some(value) => Right(value)
        case None => Left(DecodingFailure("Unsupported currency", Nil))
      }
    }

  // ----- REQUEST AND RESPONSE ENCODERS/DECODERS -----
  implicit val requestDataDecoder: Decoder[RequestData] =
    Decoder.forProduct3(FROM_FIELD_NAME, TO_FIELD_NAME, AMOUNT_FIELD_NAME) {
      (from, to, amount) => RequestData(from, to, amount)
    }

  implicit val responseDataEncoder: Encoder[ResponseData] =
    Encoder.instance { responseData =>
      Json.fromJsonObject(
        JsonObject(
          FROM_FIELD_NAME -> Json.fromString(responseData.from.toString),
          TO_FIELD_NAME -> Json.fromString(responseData.to.toString),
          AMOUNT_FIELD_NAME -> Json.fromString(responseData.amount.toString),
          CONVERTED_FIELD_NAME -> Json.fromString(responseData.converted.toString)
        )
      )
    }


  // ------------- HELPER DECODER FOR LISTS -------------------
  trait FilterValidListParsing {
    implicit def listOfValidItems[T: Decoder]: Decoder[List[T]] =
      Decoder
        .decodeList(
          Decoder[T].either(Decoder[Json]))
        .map(
          _.flatMap(_.left.toOption))
  }


  // ------- PRIVATBANK API RESPONSE DECODERS --------
  object ActualApiDecoders  extends FilterValidListParsing {
    implicit val currencyRateDecoder: Decoder[CurrencyRate] =
      Decoder.instance { c =>
        for {
          ccy <- c.get[CurrCode](CCY_PB)
          base <- c.get[CurrCode](BASE_PB)
          sale <- c.get[MoneyRepr](SALE_PB)
          buy <- c.get[MoneyRepr](BUY_PB)
        } yield CurrencyRate(base, ccy, sale, buy)
      }

    implicit val listCurrencyRateNowDecoder: Decoder[List[CurrencyRate]] =
      Decoder
        .decodeList(
          Decoder[CurrencyRate].either(Decoder[Json]))
        .map(
          _.flatMap(_.left.toOption))

    implicit val pbCurrenciesInfoDecoder: Decoder[PrivatbankApiRates] =
      Decoder.instance { c =>
        for {
          rates <- c.as[List[CurrencyRate]]
        } yield PrivatbankApiRates(rates)
      }
  }


  // ------------ ARCHIVE API VERSION --------------

  object ArchiveApiDecoders extends FilterValidListParsing {
    val CCY_PB_ARCHIVE = "currency"
    val BASE_PB_ARCHIVE = "baseCurrency"
    val BUY_PB_ARCHIVE = "purchaseRateNB"
    val SALE_PB_ARCHIVE = "saleRateNB"

    val LIST_OF_RATES_ARCHIVE = "exchangeRate"

    implicit val currencyRateDecoderArchive: Decoder[CurrencyRate] =
      Decoder.instance { c =>
        for {
          ccy <- c.get[CurrCode](CCY_PB_ARCHIVE)
          base <- c.get[CurrCode](BASE_PB_ARCHIVE)
          sale <- c.get[Double](SALE_PB_ARCHIVE).map(d => MoneyRepr.fromString(d.toString))
          buy <- c.get[Double](BUY_PB_ARCHIVE).map(d => MoneyRepr.fromString(d.toString))
        } yield CurrencyRate(base, ccy, sale, buy)
      }

    implicit val pbCurrenciesInfoDecoderArchive: Decoder[PrivatbankApiRates] =
      Decoder.instance { c =>
        for {
          rates <- c.downField(LIST_OF_RATES_ARCHIVE).as[List[CurrencyRate]]
        } yield PrivatbankApiRates(rates)
      }
  }
}

package http.clients

import cats.syntax.all._
import cats.effect._

import org.http4s._
import org.http4s.client._
import org.http4s.circe.toMessageSyntax
import org.http4s.client.dsl.Http4sClientDsl

import domain.Currencies._
import domain.ExchangeErrors._
import config.AllConfigs.PrivatbankClientConfig

trait PrivatBankClient {
  def getCurrenciesCashNow: IO[PrivatbankApiRates]

  def getCurrenciesCashlessNow: IO[PrivatbankApiRates]

  def getCurrenciesArchive(date: DateParam): IO[PrivatbankApiRates]
}

class PrivatBankClientService private(
    client: Client[IO],
    config: PrivatbankClientConfig
) extends PrivatBankClient
    with Http4sClientDsl[IO] {

  def getCurrenciesCashNow: IO[PrivatbankApiRates] =
    getCurrenciesNow(config.cashId)

  def getCurrenciesCashlessNow: IO[PrivatbankApiRates] =
    getCurrenciesNow(config.cashlessId)

  private def getCurrenciesNow(courseId: Int): IO[PrivatbankApiRates] = {
    import http.JsonCodecs.ActualApiDecoders._

    val uriString = config.uri + config.pubInfoRoute

    Uri.fromString(uriString)
      .map(_.withQueryParam("json"))
      .map(_.withQueryParam("exchange"))
      .map(_.withQueryParam("coursid", courseId))
      .liftTo[IO]
      .flatMap { uri =>
        IO(Request[IO](method = Method.GET, uri = uri))
          .flatMap { req =>
          client.run(req).use { resp =>
            if (resp.status == Status.Ok)
              resp
                .asJsonDecode[PrivatbankApiRates]
            else
              PrivatbankApiNowResponse(
                Option(resp.status.reason).getOrElse("Unknown")
              ).raiseError[IO, PrivatbankApiRates]
          }
        }
      }
  }

  def getCurrenciesArchive(dateParam: DateParam): IO[PrivatbankApiRates] = {
    import http.JsonCodecs.ArchiveApiDecoders._

    val uriString = config.uri + config.archiveRoute

    Uri.fromString(uriString)
      .map(_.withQueryParam("json"))
      .map(_.withQueryParam("date", dateParam.date))
      .liftTo[IO]
      .flatMap { uri =>
        IO(Request[IO](method = Method.GET, uri = uri))
          .flatMap { req =>
            client.run(req).use { resp =>
              if (resp.status == Status.Ok)
                resp
                  .asJsonDecode[PrivatbankApiRates]
              else
                PrivatbankApiArchiveResponse(
                  Option(resp.status.reason).getOrElse("Unknown")
                ).raiseError[IO, PrivatbankApiRates]
            }
          }
      }
  }
}

object PrivatBankClientService {
  def make(
      client: Client[IO],
      config: PrivatbankClientConfig
  ): IO[PrivatBankClientService] =
    IO.delay(
      new PrivatBankClientService(client, config)
    )
}
package services

import cats.effect.IO
import org.typelevel.log4cats.{Logger, SelfAwareStructuredLogger}
import retry._

import config.AllConfigs.RetryConfig
import domain.Currencies._
import domain.ExchangeErrors._
import http.clients.PrivatBankClient

trait CurrencyExchange {
  def convertCash(requestData: RequestData): IO[ResponseData]

  def convertCashless(requestData: RequestData): IO[ResponseData]

  def convertForDate(requestData: RequestData, date: DateParam): IO[ResponseData]
}

class CurrencyExchangeService private(
    privatBankClient: PrivatBankClient,
    cfg: RetryConfig
)(implicit logger: SelfAwareStructuredLogger[IO]) extends CurrencyExchange {

  private val retryPolicy: RetryPolicy[IO] =
    RetryPolicies.limitRetries[IO](2) join
    RetryPolicies.exponentialBackoff(cfg.delay)

  def convertCash(requestData: RequestData): IO[ResponseData] = {
    val currenciesIO = retryingOnAllErrors(
      retryPolicy,
      logError
    )(privatBankClient.getCurrenciesCashNow)

    handlePbResponse(currenciesIO, requestData)
  }

  def convertCashless(requestData: RequestData): IO[ResponseData] = {
    val currenciesIO = retryingOnAllErrors(
      retryPolicy,
      logError
    )(privatBankClient.getCurrenciesCashlessNow)

    handlePbResponse(currenciesIO, requestData)
  }

  def convertForDate(requestData: RequestData, date: DateParam): IO[ResponseData] = {
    val currenciesIO = retryingOnAllErrors(
      retryPolicy,
      logError
    )(privatBankClient.getCurrenciesArchive(date))

    handlePbResponse(currenciesIO, requestData)
  }

  def handlePbResponse(
      currenciesIO: IO[PrivatbankApiRates],
      requestData: RequestData
  ): IO[ResponseData] =
    currenciesIO.flatMap { courses =>
      convert(requestData, courses) match {
        case Some(resp) =>
          IO.delay(resp)
        case None => IO.raiseError[ResponseData](ConversionError)
      }
    }

  def convert(
      requestData: RequestData,
      courses: PrivatbankApiRates
  ): Option[ResponseData] = {

    val from = requestData.from
    val to = requestData.to
    val amount = requestData.amount

    val rates = courses.rates

    val convertedOption: Option[MoneyRepr] =
      if (from == to)
        Some(amount)
      else
        from match {
          case UAH => for {
            currRate <- rates.find(_.ccy == to)
          } yield amount / currRate.sale
          case _ => to match {
            case UAH => for {
              currRate <- rates.find(_.ccy == from)
            } yield amount * currRate.buy
            case _ => for {
              buyFromRate <- rates.find(_.ccy == from)
              saleToRate <- rates.find(_.ccy == to)
            } yield amount * buyFromRate.buy / saleToRate.sale
          }
        }


    convertedOption match {
      case Some(value: MoneyRepr) =>
        Some(ResponseData(from, to, amount, value))
      case None => None
    }
  }

  private def logError(error: Throwable, details: RetryDetails): IO[Unit] =
    Logger[IO].error(s"Error: $error. Details: $details")
}

object CurrencyExchangeService {
  def make(privatBankClient: PrivatBankClient, cfg: RetryConfig)
    (implicit logger: SelfAwareStructuredLogger[IO]): IO[CurrencyExchangeService] =
    IO.delay(
      new CurrencyExchangeService(privatBankClient, cfg)
    )
}

package domain

import scala.util.control.NoStackTrace

object ExchangeErrors {
  trait InternalErrors extends NoStackTrace
  case object ConversionError extends InternalErrors

  trait PrivatbankApiErrors extends NoStackTrace
  case class PrivatbankApiNowResponse(cause: String) extends PrivatbankApiErrors
  case class PrivatbankApiArchiveResponse(cause: String) extends PrivatbankApiErrors
}

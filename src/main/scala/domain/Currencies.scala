package domain

import java.math.MathContext

object Currencies {

  // -------- CURRENCY AND MONEY REPRESENTATION --------

  sealed trait CurrCode
  final case object UAH extends CurrCode
  final case object USD extends CurrCode
  final case object EUR extends CurrCode
  final case object RUB extends CurrCode

  object CurrCode {
    def convertString(value: String): Option[CurrCode] = value match {
      case "UAH" => Some(UAH)
      case "USD" => Some(USD)
      case "EUR" => Some(EUR)
      case "RUB" | "RUR" => Some(RUB)
      case _ => None
    }
  }

  case class MoneyRepr private (value: BigDecimal) extends AnyVal {
    def +(other: MoneyRepr): MoneyRepr =
      MoneyRepr(value + other.value)

    def -(other: MoneyRepr): MoneyRepr =
      MoneyRepr(value - other.value)

    def *(other: MoneyRepr): MoneyRepr =
      MoneyRepr(value * other.value)

    def /(other: MoneyRepr): MoneyRepr = {
      MoneyRepr(value / other.value)
    }

    override def toString: String =
      "%.7f".formatLocal(java.util.Locale.US, value)
  }

  object MoneyRepr {
    def fromString(string: String): MoneyRepr =
      MoneyRepr(BigDecimal(string, MathContext.DECIMAL128))
  }


  // -- DATE PARAMETER FOR ARCHIVE REQUESTS --
  case class DateParam private (date: String) extends AnyVal

  object DateParam {
    def fromString(str: String): DateParam =
      DateParam(str)
  }


  // --------- REQUEST PARAMETERS ----------

  case class RequestData(
      from: CurrCode,
      to: CurrCode,
      amount: MoneyRepr
  )


  // -------- RESPONSE PARAMETERS ---------

  case class ResponseData(
      from: CurrCode,
      to: CurrCode,
      amount: MoneyRepr,
      converted: MoneyRepr
  )


  // -------- PARSE API DATA TYPES --------
  case class PrivatbankApiRates(
      rates: List[CurrencyRate]
  )

  case class CurrencyRate(
      base: CurrCode,
      ccy: CurrCode,
      sale: MoneyRepr,
      buy: MoneyRepr
  )
}

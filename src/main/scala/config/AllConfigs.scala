package config

import scala.concurrent.duration.FiniteDuration

object AllConfigs {
  case class AppConfig(
      pbConfig: PrivatbankClientConfig,
      clientConfig: GeneralClientConfig,
      serverConfig: ServerConfig,
      retryConfig: RetryConfig
  )

  case class PrivatbankClientConfig(
      uri: String,
      pubInfoRoute: String,
      archiveRoute: String,
      cashId: Int,
      cashlessId: Int
  )

  case class GeneralClientConfig(
      connectTimeout: FiniteDuration,
      requestTimeout: FiniteDuration
  )

  case class ServerConfig(
      host: String,
      port: Int
  )

  case class RetryConfig(
      limit: Int,
      delay: FiniteDuration
  )
}

package config

import cats.effect._
import scala.concurrent.duration._

import AllConfigs._

object ConfigLoader {
  def load(): IO[AppConfig] =
    default

  private val default: IO[AppConfig] = {
    IO.delay(
      AppConfig(
        pbConfig =PrivatbankClientConfig(
          uri = "https://api.privatbank.ua",
          pubInfoRoute = "/p24api/pubinfo",
          archiveRoute = "/p24api/exchange_rates",
          cashId = 5,
          cashlessId = 11),
        clientConfig = GeneralClientConfig(
          connectTimeout = 2.seconds,
          requestTimeout = 2.seconds
        ),
        serverConfig = ServerConfig(
          host = "0.0.0.0",
          port = 8080
        ),
        RetryConfig(
          limit = 5,
          delay = 500.millis
        )
      )
    )
  }
}

name := "currency-converter"

version := "0.1"

scalaVersion := "2.13.7"

val http4sVersion = "0.23.6"
val catsVersion = "3.3.0"
val log4catsVersion = "2.1.1"
val logBackVersion = "1.2.7"
val catsRetryVersion = "3.1.0"

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.typelevel" %% "cats-effect" % catsVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "org.typelevel" %% "log4cats-core" % log4catsVersion,
  "org.typelevel" %% "log4cats-slf4j" % log4catsVersion,
  "ch.qos.logback" % "logback-classic" % logBackVersion,
"com.github.cb372" %% "cats-retry" % catsRetryVersion,
)